#!/usr/bin/python3
# coding: utf-8

"""This test module relies on pytest.
   
   Installation:
   
   .. code-block:: sh
   
     pip3 install pytest
"""

import pytest

from cozylib.api.permission import Permission, Type, Verb, scope

def test_permission_scope():
  p = Permission(Type.FILES)
  assert p.scope() == 'io.cozy.files'

  p = Permission(Type.FILES, verbs=Verb.ALL)
  assert p.scope() == 'io.cozy.files'
  
  with pytest.raises(Permission.InvalidValueTypeError) as e:
    p = Permission(Type.FILES, verbs=Verb.ALL, values='12')
  
  p = Permission(Type.FILES, verbs=Verb.ALL, values=['12'])
  assert p.scope() == 'io.cozy.files:ALL:12'

  p = Permission(Type.FILES, verbs=Verb.ALL, values=['12', '24'])
  assert p.scope() == 'io.cozy.files:ALL:12 io.cozy.files:ALL:24'
  
  p = Permission(Type.FILES, values=['12', '24'], selector='name')
  assert p.scope() == 'io.cozy.files:ALL:12:name io.cozy.files:ALL:24:name'

  p = Permission(Type.FILES, values=['12', '24'])
  assert p.scope() == 'io.cozy.files:ALL:12 io.cozy.files:ALL:24'

  # verbs default to ALL, values, selector
  p = Permission(Type.FILES, values=['12', '24'], selector='name')
  assert p.scope() == 'io.cozy.files:ALL:12:name io.cozy.files:ALL:24:name'

  # no value: selector ignored
  p = Permission(Type.FILES, selector='name')
  assert p.scope() == 'io.cozy.files'
  
  p = Permission(Type.FILES, verbs=Verb.GET)
  assert p.scope() == 'io.cozy.files:GET'

  p = Permission(Type.FILES, verbs=Verb.GET | Verb.POST)
  assert p.scope() == 'io.cozy.files:GET io.cozy.files:POST'

  p = Permission(Type.FILES, verbs=Verb.GET | Verb.POST)
  assert p.scope() == 'io.cozy.files:GET io.cozy.files:POST'

  p = Permission(Type.OAUTH_CLIENTS, verbs=Verb.GET | Verb.POST, values=['ab', 'cd'])
  assert p.scope() == 'io.cozy.oauth.clients:GET:ab io.cozy.oauth.clients:GET:cd io.cozy.oauth.clients:POST:ab io.cozy.oauth.clients:POST:cd'

  p = Permission(Type.OAUTH_CLIENTS, verbs=Verb.GET | Verb.POST, values=['ef', 'gh'], selector='id')
  assert p.scope() == 'io.cozy.oauth.clients:GET:ef:id io.cozy.oauth.clients:GET:gh:id io.cozy.oauth.clients:POST:ef:id io.cozy.oauth.clients:POST:gh:id'  
# -----
 