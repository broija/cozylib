# coding: utf-8

import json

from .errors import ExpiredTokenError, ForbiddenAccessError, InvalidCodeError, InvalidPathError, InvalidRefreshTokenError, RequestError
from .file import File

class Parser:
  """Cozy API response parser.

     https://github.com/cozy/cozy-stack/blob/master/docs/files.md"""

  def __init__(self):
    self.clear()
# -----
  def clear(self):
    self.json = None
    self.data = None

    self.count = 0

    self.files = {}
# -----
  def parse(self, response):
    self.clear()

    if not response:
      return False

    self.json = json.loads(response)
    print("RESPONSE:", json.dumps(self.json, indent=4))

    if response in self._token_errors:
      raise self._token_errors[response]

    if 'error' in response:
      # print("RESPONSE:", response)
      self._parse_error(response)

    self.data = self.json['data']
    included = self.json['included'] if 'included' in self.json else []

    for item in self.data:
      if type(item)==dict and item['type'] == 'io.cozy.files':
        file_id = item['id']
        file = File(file_id)

        file.__dict__.update(item['attributes'])

        self.files[file_id] = file
        print(file)

    self.count = len(self.files)

    return True
# -----
  def _parse_error(self, data):
    json_res = json.loads(data)
    errors = []
    if 'errors' in json_res:
      errors = json_res['errors']
    elif 'error' in json_res:
      errors.append(json_res['error'])

#    error = err['error']

    for err in errors:
      if 'detail' in err:
        err = err['detail']

      if err in self._errors:
        raise self._errors[err](err)
      else:
        raise RequestError(data)

Parser._errors = {
  'invalid refresh token': InvalidRefreshTokenError,
  'invalid code': InvalidCodeError,
  'Path should be absolute': InvalidPathError
}

Parser._token_errors = {
  'Expired token': ExpiredTokenError,
  'Forbidden': ForbiddenAccessError
}
