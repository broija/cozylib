# coding: utf-8

class InvalidPermissionError(Exception): pass

class AuthorizationError(Exception): pass

class MissingParameterError(Exception): pass
class MissingTokenError(Exception): pass

class UnhandledTokenTypeError(Exception): pass

# Permissions
class PermissionInvalidValueTypeError(Exception): pass

# Request errors
class InvalidCodeError(Exception): pass
class InvalidPathError(Exception): pass
class InvalidRefreshTokenError(Exception): pass
class RequestError(Exception): pass

# Token request errors
class ExpiredTokenError(Exception): pass
class ForbiddenAccessError(Exception): pass
