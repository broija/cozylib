# coding: utf-8

class File:
  """Cozy file item.

     https://github.com/cozy/cozy-stack/blob/master/docs/files.md"""
# -----
  def __init__(self, id):
    self.id = id
# -----
  def __str__(self):
    items = [
      'id[{}] :',
      '- name = {}',
      '- created = {}',
      '- updated = {}',
      '- dir-id = {}'
    ]
    params = [
      self.id,
      self.name,
      self.created_at,
      self.updated_at,
      self.dir_id
    ]

    if hasattr(self, 'size'):
      items.append('- size = {}')
      params.append(self.size)

    return "\n".join(items).format(*params)
