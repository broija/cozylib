# coding: utf-8

import http.client
import json
import pathlib
import secrets
import subprocess
import urllib.parse

from .errors import AuthorizationError, MissingParameterError, MissingTokenError, UnhandledTokenTypeError

from cozylib.utils import confirm

from cozylib.utils.auth import Server
from .token import Token

from .response import Parser

from . import permission

# ------------------------------------------------------------------------------

_DEFAULT_HOST = 'localhost'
_DEFAULT_PORT = 8080

class Client:
  """https://github.com/cozy/cozy-stack/blob/master/docs/auth.md
     https://github.com/cozy/cozy-stack/blob/master/docs/files.md
     """

  def __init__(self, server, auto_confirm=False):
    """:param output_dir: A directory to write tokens and registration files to."""

    self.server = server
    self.base_url = 'https://' + server

    self.http_client = http.client.HTTPSConnection(server)
#    self.http_client = http.client.HTTPConnection(server)

    self.parser = Parser()

    self.auto_confirm = auto_confirm

    self.client_id = None
    self.client_secret = None
    self.token = None

    self.code = None
    self.fresh_code = False

    # current token
    self._token = None
    self.tokens = {}

    self.output_dir = None
# -----
  def set_output_dir(self, output_dir):
    if output_dir and isinstance(output_dir, str):
      output_dir = pathlib.Path(output_dir)

    self.output_dir = output_dir

  def is_known_token(self, client_id):
    return client_id in self.tokens
# -----
  def is_same_token(self, client_id, token):
    if not self.is_known_token(client_id):
      return False

    return self.get_token(client_id) == token
# -----
  def add_token(self, client_id, token):
    if self.is_known_token(client_id):
      if self.tokens[client_id] == token or not self._confirm('{} token already exists. Replace?'.format(client_id)):
        return

    if not self._token:
      self._token = token

    self.tokens[client_id] = token
# -----
  def remove_token(self, client_id):
    """:return: True if token has been removed, False otherwise."""

    if is_known_token(client_id):
      del self.tokens[client_id]
      return True

    return False
# -----
  def clear_tokens(self):
    self.tokens = {}
# -----
  def get_token(self, client_id):
    if self.is_known_token(client_id):
      return self.tokens[client_id]
# -----
  def set_current_token(self, token):
    self._token = token
# -----
  def get_current_token(self):
    return self._token
# -----
  def default_token_file(self, client_id):
    result = client_id + '.json'

    if self.output_dir:
      result = self.output_dir / result

    return result
# -----
  def client_token_file_exists(self, client_id):
    return self.token_file_exists(self.default_token_file(client_id))
# -----
  def token_file_exists(self, path):
    """:param path: path to token file.
       :return: the Token, False otherwise."""

    if isinstance(path, str):
      path = pathlib.Path(path)

#    print(path)

    if path.is_file():
      try:
        return Token.from_file(path)
      except:
        pass

    return False
# -----
  def authorize(self, permissions=permission.Permission(permission.Type.FILES, permission.Verb.GET), client_id=None, host=None, port=_DEFAULT_PORT, browser=None):
    """GET /auth/authorize

       https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#get-authauthorize
       :param permissions: Things we need authorization for. Can be:
         - a Permission object
         - an iterable containing strings or Permission objects
         - a scope string

         See also: https://github.com/cozy/cozy-stack/blob/master/docs/permissions.md.

       :param client_id: if omitted, will try to use last 'register' client_id.
       :param host: HTTP server host that will receive response registration data. Default to {}.
       :param port: HTTP server port. Default to {}.
       :param browser: Web browser path to open authorization page.
       :return: An existing token code or a new code allowing to generate a new token.""".format(_DEFAULT_HOST, _DEFAULT_PORT)

    self.fresh_code = False

    if not client_id:
      client_id = self._check_param('client_id')

    if self.is_known_token(client_id) and self._confirm('Token already loaded, use existing authorization?'):
      return token.code

    token = self.client_token_file_exists(client_id)

    if token and self._confirm('Token file exists ({}), use existing authorization?'.format(token.path)):
      self.add_token(client_id, token)
      self.code = token.code

      return token.code

    if not host:
      host = _DEFAULT_HOST

    state = secrets.token_urlsafe(16)

    ptype = type(permissions)

    scope = []

    if ptype is str:
      scope.append(permissions)
    elif ptype is permission.Permission:
      scope.append(permissions.scope())
    else:
      for perm in permissions:
        scope.append( perm if type(perm) is str else perm.scope() )

    params = {'client_id': client_id,
              'response_type': 'code',
              'scope': ' '.join(scope),
              'state': state,
              'redirect_uri': 'http://{}:{}'.format(host, port)}

    authorize_url = urllib.parse.urljoin(self.base_url, 'auth/authorize') + '?' + urllib.parse.urlencode(params)

    print_link = True

    if browser:
      if isinstance(browser, str):
        browser = pathlib.Path(browser)

      if browser.is_file():
        subprocess.run([str(browser), authorize_url])
        print_link = False

    if print_link:
      print("Open this URL in a browser:\n", authorize_url)

    code = None
    with Server(state, port=port) as http_srv:
      http_srv.wait_auth_request()

      code = http_srv.get_code()

    if not code:
      raise errors.AuthorizationError('Error while retrieving authorization code...')

    self.fresh_code = True
    self.code = code

    return code
# -----
  def load_registration_string(self, registration):
    """Load application registration items from JSON string."""
    res = self._load_string(registration)

#    print(res)

    self.client_id = res['client_id']
    self.client_secret = res['client_secret']
#    self.token = res['registration_access_token']
    return res
# -----
  def load_registration_file(self, path):
    """Load application registration items from JSON file."""

    return self.load_registration_string(self._load_file(path))
# -----
  def register(self, client_name='cozycli', software_id='cozycli', registration_file=None, host=None, port=_DEFAULT_PORT):
    """POST /auth/register

       https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#post-authregister

       :param registration_file: path to registration file to write registration data in.
       :param host: HTTP server host that will receive response registration data. Default to {}.
       :param port: HTTP server port. Default to {}.""".format(_DEFAULT_HOST, _DEFAULT_PORT)

    if not host:
      host = 'localhost'

    if not registration_file:
      registration_file = client_name + '.json'

      if self.output_dir:
        registration_file = self.output_dir / registration_file

    if isinstance(registration_file, str):
      registration_file = pathlib.Path(registration_file)

    if self._ask_load_from_file(registration_file):
      return self.load_registration_file(registration_file)

    headers = {'Host': self.server,
               'Content-type': 'application/json',
               'Accept': 'application/json'}

    data = {'redirect_uris': ["http://{}:{}".format(host, port)],
            'client_name': client_name,
            'software_id': software_id}

    response = self._request('/auth/register', headers=headers, data=data)

    if response:
      registration_file.open('w').write(response)

      return self.load_registration_string(response)
# -----
  def access_token(self, client_id=None, client_secret=None, code=None, refresh=False, token_file=None):
    """POST /auth/access_token

       https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#post-authaccess_token

       :param client_id: if omitted, will try to use last 'register' client_id.
       :param client_secret: if omitted, will try to use last 'register' client_secret.
       :param code: if omitted, will try to use last 'authorize' code.
       :return: If token is unknown, a new Token is returned. If token is known, token may be refreshed according to user's choice."""

    if not client_id:
      client_id = self._check_param('client_id')

    if not client_secret:
      client_secret = self._check_param('client_secret')

    if not code:
      if not self.fresh_code:
        print("Using old code: skipping access token procedure...")
        return self.get_token(client_id)

      code = self._check_param('code')

    if not token_file:
      token_file = self.default_token_file(client_id)

    if isinstance(token_file, str):
      token_file = pathlib.Path(token_file)

    if not refresh and not self.fresh_code:
      if token_file.is_file():
        token = Token.from_file(token_file)

        if not self.is_same_token(client_id, token) and self._ask_load_from_file(token_file):

          # if token has not been added
          if not self.add_token(client_id, token):
            # return existing token
            return self.get_token(client_id)

          return token

    headers = {'Host': self.server,
               'Content-type': 'application/x-www-form-urlencoded',
               'Accept': 'application/json'}

    data = {'grant_type': 'refresh_token' if refresh else 'authorization_code',
            'client_id': client_id,
            'client_secret': client_secret}

    key = 'refresh_token' if refresh else 'code'
    data[key] = code

    response = self._request('/auth/access_token', headers=headers, data=data, json_data=False)

    if response:
      jo = json.loads(response)
      jo['code'] = code
      js = json.dumps(jo)

      token_file.open('w').write(js)

      token = Token.from_string(js)

      if not self.add_token(client_id, token):
        return self.get_token(client_id)

      return token
# ----------
# Files: https://github.com/cozy/cozy-stack/blob/master/docs/files.md
# ----------
  def dir_list(self, path, token=None):
    """GET /files/metadata

       https://github.com/cozy/cozy-stack/blob/master/docs/files.md#get-filesmetadata

       param token: Token to issue the request. If None, try to use current Token."""

    response = self._token_request('/files/metadata', token=token, params={'Path': path}, headers={'Accept': 'application/vnd.api+json'})

    self.parser.parse(response)

    return self.parser.files
# -----
# Trash: https://github.com/cozy/cozy-stack/blob/master/docs/files.md#trash
# -----
  def trash_list(self, token=None):
    """GET /files/trash

       https://github.com/cozy/cozy-stack/blob/master/docs/files.md#get-filestrash

       :param token: Token to issue the request. If None, try to use current Token."""

    response = self._token_request('/files/trash', token=token, headers={'Accept': 'application/vnd.api+json'})

    self.parser.parse(response)

    return self.parser.files
# -----
  def trash_clear(self, token=None):
    """DELETE /files/trash

       https://github.com/cozy/cozy-stack/blob/master/docs/files.md#delete-filestrash"""

    print(self._token_request('/files/trash', token=token, method='DELETE'))
# ----------
# Protected methods
# ----------
  def _token_request(self, path, token=None, params=None, headers={}, data=None, method=None):
    """Execute a request that requires a token.

       https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#how-to-use-a-token-1."""

    if not token:
      if not self._token:
        raise errors.MissingTokenError('{} {}'.format(self.server, path))

      token = self._token
      print('Token not set, using current token...')

    ttype = token.type

    if ttype != 'Bearer':
      raise errors.UnhandledTokenTypeError(ttype, self.server, path)

    headers['Authorization'] = "{} {}".format(ttype, token.access)

    response = self._request(path, params=params, headers=headers, data=data, method=method)

    return response
# -----
  def _request(self, path, params=None, headers=None, data=None, json_data=True, method=None):
    """:param data: data dict"""

    if not method:
      method = 'POST' if data else 'GET'

    print('+++++\nREQUEST:\n',method, path, '\n+++++')

    if data:
      data = json.dumps(data) if json_data else urllib.parse.urlencode(data)

    if params:
      path = path + '?' + urllib.parse.urlencode(params)

    self.http_client.request(method, path, body=data, headers=headers)

    response = self.http_client.getresponse().read().decode()

    return response
# -----
  def _check_param(self, param):
    result = getattr(self, param)

    if not result:
        raise errors.MissingParameterError(param)

    return result
# -----
  def _ask_load_from_file(self, path):
    return path.is_file() and self._confirm('{} already exists. Load (y) or overwrite (n)?'.format(path))
# -----
  def _load_file(self, path):
    if isinstance(path, str):
      path = pathlib.Path(path)

    return path.open().read()
# -----
  def _load_string(self, data):
    return json.loads(data)
# -----
  def _confirm(self, question):
    return confirm(question, auto_confirm=self.auto_confirm)
