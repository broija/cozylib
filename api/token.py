# coding: utf-8

import json

class Token:
  """Cozy API authorization token.
     
     https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#how-to-get-a-token-1 
     """
  class InvalidStringError(Exception): pass

  @classmethod
  def from_string(cls, token):
    """Load token items from JSON string."""

    res = json.loads(token)

#    print(res)

    try:
      token = Token(res['token_type'], res['access_token'], res['refresh_token'], res['scope'], res['code'])
    except KeyError:
      raise cls.InvalidStringError(token)
      
    return token
# -----
  @classmethod
  def from_file(cls, path):
    with open(path, 'r') as token_file:
      token = cls.from_string(token_file.read())

      token.path = path

      return token
# -----
  def __init__(self, ttype, access, refresh, scope, code):
    """:param ttype: token type."""

    ttype = ttype.title()

    self.type = ttype

    self.access = access
    self.refresh = refresh

    self.scope = scope.split()
    
    self.code = code
    
    self.path = None
# -----
  def __str__(self):
    return '- TOKEN[{}]\n- ACCESS[{}]\n- REFRESH[{}]\n- SCOPE:\n{}'.format(self.type, self.access, self.refresh, '\n'.join(self.scope))
# -----
  def __eq__(self, other):
    return self.type == other.type and self.access == other.access and self.refresh == other.refresh and self.scope == other.scope and self.code == other.code