# coding: utf-8

import enum

class Verb(enum.Flag):
  """https://github.com/cozy/cozy-stack/blob/master/docs/permissions.md#verbs"""

  GET = enum.auto()
  POST = enum.auto()
  PUT = enum.auto()
  PATCH = enum.auto()
  DELETE = enum.auto()

  ALL = GET | POST | PUT | PATCH | DELETE

# -----

class Type(enum.Enum):
  """https://github.com/cozy/cozy-stack/blob/master/docs/permissions.md#type"""

  FILES = 'io.cozy.files'
  APPS = 'io.cozy.apps'
  SETTINGS = 'io.cozy.settings'
  JOBS = 'io.cozy.jobs'
  TRIGGERS = 'io.cozy.triggers'
  OAUTH_CLIENTS = 'io.cozy.oauth.clients'

# -----

_ATTR_SEP = ':'
_PERM_SEP = ' '

class Permission:
  """Cozy permission

     https://github.com/cozy/cozy-stack/blob/master/docs/permissions.md"""

  def __init__(self, ptype, verbs=None, values=None, selector=None):
    """:param verbs: If not set, default to ALL.
       :param values: list of target values affecetd by permission. If no selector is defined, it is generally ids."""

    if values and not isinstance(values, list):
      raise errors.PermissionInvalidValueTypeError(type(values))

    self._type = ptype

    if not verbs:
      verbs = Verb.ALL

    self._verbs = verbs

    self._values = values
    self._selector = selector

    scope = ptype.value
    scopes = []

    items = [None]

    if values:
      items.append(None)

      if selector:
        items.append(selector)

    if verbs & Verb.ALL == Verb.ALL:
      if values:
        items[0] = scope + _ATTR_SEP + verbs.name

        for value in values:
          items[1] = value

          scopes.append(_ATTR_SEP.join(items))
    else:
      tmp = []

      if verbs & Verb.GET:
        tmp.append(scope + _ATTR_SEP + verbs.GET.name)

      if verbs & Verb.POST:
        tmp.append(scope + _ATTR_SEP + verbs.POST.name)

      if verbs & Verb.PUT:
        tmp.append(scope + _ATTR_SEP + verbs.PUT.name)

      if verbs & Verb.PATCH:
        tmp.append(scope + _ATTR_SEP + verbs.PATCH.name)

      if verbs & Verb.DELETE:
        tmp.append(scope + _ATTR_SEP + verbs.DELETE.name)

      for t in tmp:
        items[0] = t

        if values:
          for value in values:
            items[1] = value

            print(items)
            scopes.append(_ATTR_SEP.join(items))

        else:
          scopes.append(_ATTR_SEP.join(items))

    if not scopes:
      scopes.append(scope)

    self._scope = _PERM_SEP.join(scopes)

  def scope(self):
    """https://github.com/cozy/cozy-stack/blob/master/docs/permissions.md#inline"""

    return self._scope

  __str__ = scope
# -----

def scope(permissions):
  return ' '.join([perm.scope() for perm in permissions])

