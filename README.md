# cozylib #

Far from complete Python interface for Cozy API.

## handled requests ##

### auth ###

- GET /auth/authorize
- POST /auth/access_token
- POST /auth/register

### files ###

- DELETE /files/trash
- GET /files/trash

Example:

```py
import argparse
import sys

import cozylib.api
import cozylib.api.token
import cozylib.api.permission

argparser = argparse.ArgumentParser()

group = argparser.add_argument_group('server')
group.add_argument('--server', '-s', help='Full domain. Ex: claude.mycozy.cloud.')
group.add_argument('--user', '-u', help='Cozy user to be joined with --base-domain argument. Ex: claude.')
group.add_argument('--base-domain', '-b', default='mycozy.cloud', help='Base domain, to be joined with user. Ex: mycozy.cloud.')

argparser.add_argument('--port', '-p', type=int, default=8080, help='Local port used for token retrieval.')
argparser.add_argument('--client', default="cozycli")
argparser.add_argument('--software', default="cozycli")
argparser.add_argument('--registration-file', help='Path to client application registration file.')
argparser.add_argument('--browser-path', '-B', help='Web browser path.')
argparser.add_argument('--refresh-token', '-r', action='store_true', help='Refresh token.')

args = argparser.parse_args()

server = None

if args.server:
  server = args.server
else:
  if args.user and args.base_domain:
    server = args.user + '.' + args.base_domain

if not server:
  print("Please specify server arguments.")
  sys.exit(1)

api = cozylib.api.Client(server)

api.register(client_name=args.client, software_id=args.software, registration_file=args.registration_file, port=args.port)

code = api.authorize(permissions=[cozylib.api.permission.Permission(cozylib.api.permission.Type.FILES, cozylib.api.permission.Verb.GET),
                                  'io.cozy.files:DELETE'],
                     port=args.port, browser=args.browser_path)
token = api.access_token(refresh=args.refresh_token)

trash_items = api.trash_list()

for item in trash_items.values():
  print(item)
  
api.trash_clear()
```

## references ##

- authorization: https://github.com/cozy/cozy-stack/blob/master/docs/auth.md
- authorization shell script: https://forum.cozy.io/t/comment-se-procurer-une-cle-client-pour-lapi/4648/10
- app. registering: https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#post-authregister
- desktop app. registering: https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#native-apps-on-desktop
- app. permissions: https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#get-authauthorize
- retrieving tokens: https://github.com/cozy/cozy-stack/blob/master/docs/auth.md#post-authaccess_token
- file API: https://github.com/cozy/cozy-stack/blob/master/docs/files.md
- services: https://github.com/cozy/cozy-stack/blob/master/docs/README.md#list-of-services
- permissions: https://github.com/cozy/cozy-stack/blob/master/docs/permissions.md
