# coding: utf-8

import socketserver

from .handler import Handler

class Server(socketserver.TCPServer):
  def __init__(self, state, port):
    """:param state: Randomly generated string that will be checked against Cozy response.
       :param port: """
    super().__init__( ("", port), Handler )
    
    self.state = state
    self.access_code = None
    self.code = None
# -----
  def get_state(self):
    return self.state
# -----
  def get_access_code(self):
    return self.access_code
# -----
  def set_access_code(self, code):
    self.access_code = code
# -----
  def get_code(self):
    return self.code
# -----
  def set_code(self, code):
    self.code = code
# -----
  def wait_auth_request(self):
    print( 'Serving on {}...'.format( self.server_address[1] ) )

    while True:
      self.handle_request()
      
      if self.code:
        break
