# coding: utf-8

import http.server
import re
import urllib.parse

class Handler(http.server.BaseHTTPRequestHandler):
  def do_GET(self):
    print('Incoming GET request...')
    match_res = re.match('GET .+(access_code=.*code=.*state=.*) HTTP/.*', self.requestline)

    if match_res:
      items = dict( urllib.parse.parse_qsl( match_res.group(1) ) )

      print(items)

      if self.server.get_state() == items['state']:
        print('State match...')

        self.server.set_access_code( items['access_code'] )
        self.server.set_code( items['code'] )
      
        self.send_response(200, message='OK\n\nOK')
        self.end_headers()

        self.close_connection = True
# -----
